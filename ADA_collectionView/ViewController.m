//
//  ViewController.m
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import "ViewController.h"
#import "TableViewCellWithCollectionView.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"ADA CollectionView in TableViewCell";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"tableCell"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3)
    {
        TableViewCellWithCollectionView *cellWithCollectionView = [tableView dequeueReusableCellWithIdentifier:@"cellWithCollectionView" forIndexPath:indexPath];
        return cellWithCollectionView;
    }
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    return cell;
}




@end
