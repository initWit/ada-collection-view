//
//  MyCollectionView.m
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import "MyCollectionView.h"

@implementation MyCollectionView

-(NSInteger)accessibilityElementCount
{
    return self.accessible_count;
}

@end
