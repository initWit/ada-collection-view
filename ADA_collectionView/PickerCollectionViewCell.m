//
//  PickerCollectionViewCell.m
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import "PickerCollectionViewCell.h"
#import "MyCollectionView.h"

@implementation PickerCollectionViewCell

- (void)accessibilityElementDidBecomeFocused
{
    MyCollectionView *collectionView = (MyCollectionView *)self.superview;
    [collectionView scrollToItemAtIndexPath:[collectionView indexPathForCell:self] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally|UICollectionViewScrollPositionCenteredVertically animated:NO];
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
}


@end
