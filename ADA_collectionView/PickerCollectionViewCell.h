//
//  PickerCollectionViewCell.h
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
