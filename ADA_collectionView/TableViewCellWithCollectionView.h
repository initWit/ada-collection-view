//
//  TableViewCellWithCollectionView.h
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCollectionView.h"

@interface TableViewCellWithCollectionView : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>
{
    
}

@property(nonatomic, weak) IBOutlet MyCollectionView *myCollectionView;

@end
