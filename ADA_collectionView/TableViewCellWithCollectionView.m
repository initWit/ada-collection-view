//
//  TableViewCellWithCollectionView.m
//  ADA_collectionView
//
//  Created by Figueras, Robert on 3/13/18.
//  Copyright © 2018 Figueras, Robert. All rights reserved.
//

#import "TableViewCellWithCollectionView.h"
#import "PickerCollectionViewCell.h"

@implementation TableViewCellWithCollectionView

-(id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

-(void)setup
{
    self.myCollectionView.dataSource = self;
    self.myCollectionView.delegate = self;
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.myCollectionView.collectionViewLayout = flow;
    self.myCollectionView.accessible_count = 1;
}




/*------------------------------------------------------------- Collection View -------------------------------------------------------------*/



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PickerCell" forIndexPath:indexPath];
    cell.isAccessibilityElement = YES;
    cell.accessibilityTraits = UIAccessibilityTraitButton;
    cell.accessibilityLabel = [NSString stringWithFormat:@"This is cell %ld", indexPath.row];
    
    cell.label.text = [NSString stringWithFormat:@"%ld", indexPath.row];

    
    cell.backgroundColor = [UIColor redColor];
    return cell;
}

@end

